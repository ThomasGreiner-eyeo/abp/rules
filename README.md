# @adblockinc/rules

@adblockinc/rules is an NPM package for retrieving, storing and providing filter
lists from various sources for use in AdBlock and Adblock Plus.
